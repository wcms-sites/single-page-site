Steps to set up a single page site:

Build this makefile in the site's folder.

Clear the site caches.

Enable the responsive theme.

Enable and set default the marketing theme.

Disable unused content types. Do one at a time to avoid primary key errors:
drush dis uw_ct_bibliography -y
drush dis uw_ct_blog -y
drush dis uw_ct_contact -y
drush dis uw_ct_custom_listing -y
drush dis uw_ct_embedded_timeline -y
drush dis uw_ct_event -y
drush dis uw_ct_home_page_banner -y
drush dis uw_ct_image_gallery -y
drush dis uw_ct_news_item -y
drush dis uw_ct_opportunity -y
drush dis uw_ct_person_profile -y
drush dis uw_ct_project -y
drush dis uw_ct_promo_item -y
drush dis uw_ct_service -y
drush dis uw_ct_web_form -y

Delete any content types provided by the modules you disabled above.

Do a "drush updb".

Enable the uw_ct_single_page_home module.

Do a "drush fra".

Create and publish a “single page home” piece of content, which will become the home page.

Delete the “About” and “Home” web pages.

In Configuration -> Site information, set the default home page to your newly created page (node/3 if you followed these instructions exactly).

